img2scad
========

[![Pipeline status](https://gitlab.com/victor-engmark/img2scad/badges/master/pipeline.svg)](https://gitlab.com/victor-engmark/img2scad/commits/master)

Convert black and white images to OpenSCAD structures.

Installation / upgrade: sudo pip2 install --upgrade img2scad

Dependencies: Python Imaging Library (PIL)
